Name:           python3-sabctools
Version:        8.2.5
Release:        1%{?dist}

Summary:        C implementations of functions for use within SABnzbd
License:        GPLv2
URL:            https://github.com/sabnzbd/sabctools/

Source0: https://github.com/sabnzbd/sabctools/releases/download/v%{version}/sabctools-%{version}.tar.gz

BuildRequires: gcc-c++
BuildRequires: python3-build
BuildRequires: python3-devel
BuildRequires: python3-installer
BuildRequires: python3-rpm-macros
BuildRequires: python3-setuptools
BuildRequires: python3-wheel


%description
C implementations of functions for use within SABnzbd


%prep
%setup -q -n sabctools-%{version}


%build
%py3_build


%install
%py3_install


%files
%license LICENSE.md
%doc README.md
%{_libdir}/python%{python3_version}/site-packages/sabctools*


%changelog
%autochangelog
